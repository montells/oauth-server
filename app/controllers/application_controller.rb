class ApplicationController < ActionController::Base
  before_action :authenticate_user!, except: :me

  def me
    render json: User.find(doorkeeper_token.resource_owner_id).as_json
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || root_url
  end
end
