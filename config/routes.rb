Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users
  root 'users#index'
  resources :users, path: 'members'

  get '/me', to: 'application#me'
end
